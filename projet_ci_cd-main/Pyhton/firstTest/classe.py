from typing import List

from student import Student


class Classe:
    def __init__(self,name) -> None:
        self.name = name
        self.students = []

    def save(self):
        return
    
    def saveStudent(self, student) -> List[Student]:
        for existing_student in self.students:
            if existing_student.number == student.number:
                raise Exception(f"l\'étudiant numero {student.number} existe deja dans la classe.")
                
        self.students.append(student)
        return self.students



    def getSize(self):
        return len(self.students)

    
        
    def isNumberTaken(self, numero_pris: int) -> bool:    
        for student in self.students:
            if student.number == numero_pris:
                return True 
        return False
    

 
      
    